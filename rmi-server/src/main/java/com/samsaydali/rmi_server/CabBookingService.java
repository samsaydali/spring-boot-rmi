package com.samsaydali.rmi_server;

import com.samsaydali.rmischema.Book;

public interface CabBookingService {
    public Book book(String name);
}
