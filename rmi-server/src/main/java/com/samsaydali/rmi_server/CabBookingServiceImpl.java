package com.samsaydali.rmi_server;

import com.samsaydali.rmischema.Book;

public class CabBookingServiceImpl implements CabBookingService {

    private static int count = 0;

    @Override
    public Book book(String name) {
        return new Book(++count, name);
    }

}
