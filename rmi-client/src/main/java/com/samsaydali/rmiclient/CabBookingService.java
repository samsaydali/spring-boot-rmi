package com.samsaydali.rmiclient;

import com.samsaydali.rmischema.Book;

public interface CabBookingService {
    public Book book(String name);
}
