package com.samsaydali.rmiclient;

import com.samsaydali.rmischema.Book;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

@SpringBootApplication
public class RmiClientApplication {
    @Bean
    RmiProxyFactoryBean service() {
        RmiProxyFactoryBean rmiProxyFactory = new RmiProxyFactoryBean();
        rmiProxyFactory.setServiceUrl("rmi://localhost:1099/CabBookingService");
        rmiProxyFactory.setServiceInterface(CabBookingService.class);
        return rmiProxyFactory;
    }


    public static void main(String[] args) {
        CabBookingService service = SpringApplication
                .run(RmiClientApplication.class, args).getBean(CabBookingService.class);
        Book b = service.book("My Book");
        System.out.println(
            b
        );
    }

}
