package com.samsaydali.rmischema;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RmiSchemaApplication {

    public static void main(String[] args) {
        SpringApplication.run(RmiSchemaApplication.class, args);
    }

}
